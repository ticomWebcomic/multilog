/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.hash.multilog;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author draygoza
 */
public class App {
    static private final Logger LOGGER = Logger.getLogger("mx.hash.multilog.App");
    
    static public void main(String[] args) {
        try {
            HashMap<String, Handler> logs = Handlers.obtenerLogs();
            
            LOGGER.addHandler( logs.get("0") );
            LOGGER.log(Level.INFO, "Esto se debe haber escrito en el log general de la aplicacion");
            LOGGER.removeHandler( logs.get("0") );
            
            LOGGER.addHandler( logs.get("1") );
            LOGGER.log(Level.INFO, "Operacion en terminal 1");
            LOGGER.log(Level.INFO, "Mas operaciones a la 1");
            LOGGER.removeHandler( logs.get("1") );
            
            LOGGER.addHandler( logs.get("2") );
            LOGGER.log(Level.INFO, "Operacion en terminal 2");
            LOGGER.log(Level.INFO, "Cosas a la 2");
            LOGGER.removeHandler( logs.get("2") );
            
            LOGGER.addHandler( logs.get("3") );
            LOGGER.log(Level.INFO, "Operacion en terminal 3");
            LOGGER.log(Level.INFO, "Esto debe estar en log 3");
            LOGGER.removeHandler( logs.get("3") );
            
            LOGGER.addHandler( logs.get("4") );
            LOGGER.log(Level.INFO, "Operacion en terminal 4");
            LOGGER.log(Level.INFO, "4 4 4 4 4 4 4 4 4 4 4");
            LOGGER.removeHandler( logs.get("4") );
            
            LOGGER.addHandler( logs.get("1") );
            LOGGER.log(Level.INFO, "Error en terminal 1");
            LOGGER.log(Level.INFO, "Mas errores a la 1");
            LOGGER.removeHandler( logs.get("1") );
            
            Handlers.cerrarLogs();
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
